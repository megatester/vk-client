//
//  ViewController.swift
//  VK-Client
//
//  Created by Алексей Ляшенкр on 17.01.18.
//  Copyright © 2018 Алексей Ляшенкo. All rights reserved.
//

import UIKit
import SwiftyVK

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Log out user
        VK.sessions.default.logOut()
        
        // Log in user
        VK.sessions.default.logIn(
            onSuccess: { _ in
                // Start working with SwiftyVK session here
                print("logged in is successed...")
        },
            onError: { _ in
                // Handle an error if something went wrong
                print("logged in is failure...")
        }
        )
    }
}

